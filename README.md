# Algebraic Geometry I

Typed lecture/study notes for *Algebraic Geometry I* at the university of Bonn, held by Dr. Ian Gleason.
This is jointly maintained by `@Gargantuar314` and `@JulianVoe`.
Currently, `@Gargantuar314` is responsible for the Monday lectures, whereas `@JulianVoe` for the Thursday lectures.
Since we work on different systems, a `header.tex` can be customized as desired and will not be synchronised with this repository.

## Conventions

Variable names are almost always lower case.
We use capital letters for ideals instead of Fraktur.